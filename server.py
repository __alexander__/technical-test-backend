# Run with "python server.py"

from notes.api import build_app

app = build_app()

app.run(host='localhost', port=8000)
