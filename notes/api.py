from bottle import Bottle
from bottle import response, request

from .models import Note, User
from .plugins import check_api_key
from .schemas import NoteSchema


def build_app():
    app = Bottle()

    app.install(check_api_key)

    @app.hook('after_request')
    def enable_cors():
        """
        You need to add some headers to each request.
        Don't use the wildcard '*' for Access-Control-Allow-Origin in production.
        """
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
        response.headers[
            'Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token, X-API-Key'

    @app.post('/api/notes/new/', method=['OPTIONS', 'POST'])
    def create_note():
        if request.method == 'OPTIONS':
            return {}

        noteSchema = NoteSchema()
        title = request.forms.get('title')
        content = request.forms.get('content')
        result, errors = noteSchema.load({'title': title, 'content': content})
        if errors:
            response.status = 500
            return {
                'errors': errors
            }

        note = Note(
            title=title,
            content=content
        )
        note.save()

        response.headers['Content-Type'] = 'application/json'
        response.status = 201

        return noteSchema.dump(note).data

    @app.route('/api/notes/list/', method=['OPTIONS', 'GET'])
    def notes_list():
        if request.method == 'OPTIONS':
            response.status = 200
            return {}

        notes = Note.select()
        notes_array = []
        for note in notes:
            notes_array.append({'title': note.title, 'content': note.content})

        response.headers['Content-Type'] = 'text/plain'
        noteSchema = NoteSchema(many=True)
        return {'data': noteSchema.dump(notes).data}

    @app.post('/api/user/new/')
    def create_user():
        if request.method == 'OPTIONS':
            return {}
        # userSchema = UserSchema
        username = request.forms.get('username')
        password = request.forms.get('password')

        response.headers['Content-Type'] = 'application/json'
        response.status = 201
        user = User(
            username=username
        )
        user.set_password(password)

        return {'user': username}

    return app
