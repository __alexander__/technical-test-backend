import datetime
from hashlib import md5

from peewee import *

db = SqliteDatabase('notes.db')
md5_encoding = 'utf-8'


class Note(Model):
    title = CharField(max_length=128)
    content = TextField()
    timestamp = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = db


class User(Model):
    username = CharField(max_length=128, unique=True)
    password = CharField()

    class Meta:
        database = db

    def set_password(self, password):
        self.password = md5(password.encode(md5_encoding)).hexdigest()
        self.save()

    def check_password(self, password):
        return self.password == md5(password.encode(md5_encoding)).hexdigest()


if __name__ == '__main__':
    db.create_tables([User], safe=True)
