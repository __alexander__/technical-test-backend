from marshmallow import Schema, fields, validate


class NoteSchema(Schema):
    title = fields.Str(required=True, validate=validate.Length(min=1))
    content = fields.Str(required=True, validate=validate.Length(min=1))
    timestamp = fields.DateTime()


class UserSchema(Schema):
    username = fields.Str(required=True, validate=validate.Length(min=4))
    password = fields.Str(required=True, validate=validate.Length(min=6))
