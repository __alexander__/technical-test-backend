from bottle import request, response

API_KEYS = ['api1428', 'api2857']  # TODO: Crear un model para los API KEYS


def check_api_key(callback):
    def wrapper(*args, **kwargs):
        if request.method in ('GET', 'POST') and request.headers.get('X-API-Key') not in API_KEYS:
            response.status = 401
            return {'error': 'API KEY no autorizado'}
        body = callback(*args, **kwargs)
        return body

    return wrapper
